const ee = require('lagertha').eventemitter;
const codes = require('lagertha/src/auth/session');
const uuid = require('lagertha').hash.uuid;

var __sessions__ = {};

ee.on('add-session', function (message) {
  __sessions__[message.body.session] = message.body.session;
});

module.exports = function (message) {
  switch(message.header.method) {
  case 'auth':
    message.header.code = codes.AUTH_NOK;

    if (__sessions__[message.body] === message.body) {
      delete __sessions__[message.body];
      message.body = { session: uuid() };
      __sessions__[message.body.session] = message.body.session;
      message.header.code = codes.AUTH_OK;
    }

    return message;
    break;
  case 'detroy':
    console.log(message);
    delete __sessions__[message.body];
    return message;
    break;
  default:
    return message;
  }
};
