const ee = require('lagertha').eventemitter;
const uuid = require('lagertha').hash.uuid;
const codes = require('lagertha').uprofile.user;

const __tokens__ = {};

module.exports = function (message) {
  switch(message.header.method) {
  case 'user_auth':
    message.header.code = codes.AUTH_NOK;

    if (__tokens__[message.body.client_token] === message.body.user_token) {
      message.header.code = codes.AUTH_OK;
      message.body = { session: uuid() };
      ee.trigger('add-session', message);
    }

    return message;
    break;
  case 'user_gen_token':
    message.body = {
      client_token: uuid(),
      type: message.body.type,
      username: 'MockUser'
    };

    var token = uuid();

    console.log('\nTOKEN: ', token, '\n');
    __tokens__[message.body.client_token] =  token;
    return message;
    break;
  default:
    return message;
  }
};
