"use strict";
const WebSocketServer = require('ws').Server;
const server = new WebSocketServer({ port: 8088 });
const utcode = require('lagertha').parser.utcode;

const ACTIONS = {
  'raid.welcome': require('./actions/welcome'),
  uprofile: require('./actions/uprofile'),
  session: require('./actions/session')
};

server.on('error', (err) => {
  console.error(err);
});

server.on('connection', (socket) => {

  socket.on('message', (message) => {
    try {
      message = utcode.decode(message);
      console.log('[Message:action]:', message.header.action);
      console.log('[Message:method]:', message.header.method);
      let response = ACTIONS[message.header.action](message);
      console.log('[Message:response]:\n', JSON.stringify(response, null, 2));
      socket.send(utcode.encode(response));
    } catch (e) {
      console.log(e, message);
      const error = utcode.encode({
        header: {
          action: 'server',
          method: 'error'
        },
        body: {
          error: e.toString()
        }
      });
      socket.send(error);
    }
  });

  socket.on('close', (socket) => {
    console.log('[Close]');
  });

  var welcome = utcode.encode({
    header: {
      action:  'raid.welcome',
      method: 'who'
    }
  });
  socket.send(welcome);

  console.log('[Connection]');
});

console.log('Running server on port 8088');
